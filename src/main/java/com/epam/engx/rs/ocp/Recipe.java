package com.epam.engx.rs.ocp;

public interface Recipe {
    Cup make();
}
