package com.epam.engx.rs.ocp;

public interface CupFactory {
    Cup getCup();
}
